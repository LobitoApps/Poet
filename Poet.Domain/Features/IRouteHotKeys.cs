﻿using System;
using WindowsInput.Native;
using LobitoApps.Poet.Domain.Keyboard;

namespace LobitoApps.Poet.Domain.Features
{
    public interface IRouteHotKeys
    {
        void Bind(VirtualKeyCode key, Action<VirtualKeyCode> action, KeyAction was = KeyAction.Pressed);
    }
}