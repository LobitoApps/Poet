﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using WindowsInput.Native;
using LobitoApps.Poet.Domain.Keyboard;
using LobitoApps.Poet.Domain.RNG;

namespace LobitoApps.Poet.Domain.Features
{
    public class KeySpamMacro: IUserInterfaceFeature
    {
        private const int MIN_PRESS_DELAY = 13;
        private const int MAX_PRESS_DELAY = 23;

        private readonly PoetUI poetUI;
        private readonly IKeyboard keyboard;
        private readonly IRandomNumberGenerator rng;

        private IReadOnlyCollection<VirtualKeyCode> triggers;
        private IReadOnlyCollection<VirtualKeyCode> keys;
        private VirtualKeyCode? toggleKey;
        private IEnumerable<VirtualKeyCode> currentKeys;
        private KeyAction firesOn = KeyAction.Pressed;

        private static readonly Func<KeySpamMacro, KeySpamMacro> DontMissAny = _ => _;
        private Func<KeySpamMacro, KeySpamMacro> randomlyMissKeys = DontMissAny;
        private Func<KeySpamMacro, KeySpamMacro> randomlySwapKeys = DontMissAny;

        private static readonly Func<KeySpamMacro, VirtualKeyCode, KeySpamMacro> RemoveNone = (_, k) => _;
        private readonly Func<KeySpamMacro, VirtualKeyCode, KeySpamMacro> removePressedKey = RemoveNone;

        private bool IsFlipFlop() => toggleKey != null;

        public KeySpamMacro(PoetUI poetUI, IKeyboard keyboard, IRandomNumberGenerator rng)
        {
            this.poetUI = poetUI;
            this.keyboard = keyboard;
            this.rng = rng;
        }

        public KeySpamMacro BindTo(VirtualKeyCode singleKey)
        {
            triggers = new[] { singleKey };
            return this;
        }

        public KeySpamMacro BindTo(IReadOnlyCollection<VirtualKeyCode> triggerKeys)
        {
            triggers = triggerKeys;
            return this;
        }

        public KeySpamMacro KeySequence(IReadOnlyCollection<VirtualKeyCode> sequenceKeys)
        {
            keys = sequenceKeys;
            ResetSequence();

            return this;
        }

        public KeySpamMacro SingleKey(VirtualKeyCode key)
        {
            keys = new [] { key };
            ResetSequence();

            return this;
        }

        public KeySpamMacro FlipFlop(VirtualKeyCode flip, VirtualKeyCode flop)
        {
            keys = new[] { flip };
            toggleKey = flop;

            ResetSequence();

            return this;
        }

        private void ResetSequence()
        {
            currentKeys = keys.ToArray();
        }

        private void Modify(IEnumerable<VirtualKeyCode> updatedKeySequence)
        {
            currentKeys = updatedKeySequence;
        }

        public KeySpamMacro RandomlyMissKeys(int chanceMissFirst = 0, int chanceMissLast = 0, int chanceMissRandom = 0)
        {
            randomlyMissKeys = macro =>
            {
                if (rng.Between(1, 100) <= chanceMissRandom)
                {
                    RemoveRandomKey(macro);
                }
                else if (rng.Between(1, 100) <= chanceMissLast)
                {
                    RemoveLastKey(macro);
                }
                else if (rng.Between(1, 100) <= chanceMissFirst)
                {
                    RemoveFirstKey(macro);
                }

                return macro;
            };

            return this;
        }

        private static void RemoveFirstKey(KeySpamMacro keySpamMacro)
        {
            keySpamMacro.Modify(keySpamMacro.currentKeys.Skip(1));
        }

        private void RemoveLastKey(KeySpamMacro keySpamMacro)
        {
            keySpamMacro.Modify(keySpamMacro.currentKeys.Take(currentKeys.Count() - 1));
        }

        private static void RemoveRandomKey(KeySpamMacro keySpamMacro)
        {
            var r = keySpamMacro.currentKeys.Random();
            keySpamMacro.Modify(keySpamMacro.currentKeys.Where(k => k != r));
        }

        public KeySpamMacro RandomlySwapKeys(int chanceSwapFirst = 0, int chanceSwapSecond = 0, int chanceSwapLast = 0)
        {
            randomlySwapKeys = macro =>
            {
                Chance(chanceSwapFirst, 0, 1);
                Chance(chanceSwapSecond, 1, 2);
                Chance(chanceSwapLast, 2, currentKeys.Count() - 1);

                void Chance(int chance, int p1, int p2)
                {
                    if (chance > 0 && rng.Between(1, 100) <= chance)
                    {
                        currentKeys = Swap(p1, p2);
                    }
                }

                IEnumerable<VirtualKeyCode> Swap(int p1, int p2)
                {
                    var m = currentKeys.ToArray();

                    var p = m[p1];
                    m[p1] = m[p2];
                    m[p2] = p;

                    return m;
                }

                return macro;
            };

            return this;
        }

        public KeySpamMacro FiresWhen(KeyAction was)
        {
            firesOn = was;
            return this;
        }

        public PoetUI Install()
        {
            triggers.Each(k => poetUI.Bind(k, Execute, firesOn));
            return poetUI;
        }

        private void Execute(VirtualKeyCode pressedKey)
        {
            if (IsFlipFlop())
            {
                FlipFlop(pressedKey);
            }
            else
            {
                Press(pressedKey);
            }
        }

        private void Press(VirtualKeyCode pressedKey)
        {
            randomlySwapKeys(this);
            randomlyMissKeys(this);
            removePressedKey(this, pressedKey);

            currentKeys.Each(k =>
            {
                keyboard.PressKey(k, rng.Between(MIN_PRESS_DELAY, MAX_PRESS_DELAY));
            });

            ResetSequence();
        }

        private void FlipFlop(VirtualKeyCode pressedKey)
        {
            Debug.Assert(toggleKey != null, nameof(toggleKey) + " != null");

            var flip = keys.Single();
            keyboard.PressKey(flip, rng.Between(MIN_PRESS_DELAY, MAX_PRESS_DELAY));

            keys = new[] { toggleKey.Value };
            toggleKey = flip;
            ResetSequence();
        }
    }
}