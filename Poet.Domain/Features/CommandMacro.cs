﻿using System.Collections.Generic;
using WindowsInput.Native;
using LobitoApps.Poet.Domain.Keyboard;

namespace LobitoApps.Poet.Domain.Features
{
    public class CommandMacro : IUserInterfaceFeature
    {
        private readonly PoetUI poetUI;
        private readonly IKeyboard keyboard;
        private IReadOnlyCollection<VirtualKeyCode> triggers;
        private string commandText;

        public CommandMacro(PoetUI poetUI, IKeyboard keyboard)
        {
            this.poetUI = poetUI;
            this.keyboard = keyboard;
        }

        public CommandMacro BindTo(VirtualKeyCode singleKey)
        {
            triggers = new[] { singleKey };
            return this;
        }

        public CommandMacro CommandText(string text)
        {
            commandText = text;
            return this;
        }

        public PoetUI Install()
        {
            triggers.Each(k => poetUI.Bind(k, Execute, KeyAction.Pressed));
            return poetUI;
        }

        private void Execute(VirtualKeyCode pressedKey)
        {
            keyboard.PressKey(VirtualKeyCode.RETURN);
            keyboard.Type(commandText);
            keyboard.PressKey(VirtualKeyCode.RETURN);
        }
    }
}