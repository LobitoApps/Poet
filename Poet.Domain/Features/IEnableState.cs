﻿namespace LobitoApps.Poet.Domain.Features
{
    public interface IEnableState
    {
        bool IsEnabled();
        void Enable();
        void Disable();
        void Toggle();
    }
}