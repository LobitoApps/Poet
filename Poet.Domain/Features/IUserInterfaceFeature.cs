﻿namespace LobitoApps.Poet.Domain.Features
{
    public interface IUserInterfaceFeature
    {
        PoetUI Install();
    }
}