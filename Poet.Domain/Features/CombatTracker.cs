﻿using System;
using System.Threading;

namespace LobitoApps.Poet.Domain.Features
{
    public sealed class CombatTracker
    {
        private int delay;
        private readonly PoetUI ui;
        private readonly IDispatcher dispatcher;
        private bool inCombat;

        Timer combatTimer;
        private Action onEnteredCombatAction;
        private Action onLeftCombatAction;

        public CombatTracker(PoetUI ui, IDispatcher dispatcher)
        {
            this.ui = ui;
            this.dispatcher = dispatcher;
        }

        private void CombatEnded(object state)
        {
            inCombat = false;
            dispatcher.BeginInvoke(onLeftCombatAction);
        }

        public void CombatActionDetected()
        {
            combatTimer.Change(delay, Timeout.Infinite);
            if (inCombat) return;

            inCombat = true;
            dispatcher.BeginInvoke(onEnteredCombatAction);
        }

        public CombatTracker Delay(int exitCombatMilliseconds)
        {
            delay = exitCombatMilliseconds;
            return this;
        }

        public CombatTracker WhenEnteredCombat(Action action)
        {
            onEnteredCombatAction = action;
            return this;
        }

        public CombatTracker WhenLeftCombat(Action action)
        {
            onLeftCombatAction = action;
            return this;
        }

        public PoetUI Install()
        {
            combatTimer = new Timer(CombatEnded, null, delay, Timeout.Infinite);
            return ui;
        }
    }
}