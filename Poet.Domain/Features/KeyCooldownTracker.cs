﻿using System;
using System.Threading;
using System.Threading.Tasks;
using WindowsInput.Native;
using LobitoApps.Poet.Domain.Keyboard;

namespace LobitoApps.Poet.Domain.Features
{
    public sealed class KeyCooldownTracker: IUserInterfaceFeature
    {
        private readonly PoetUI ui;
        private readonly IDispatcher dispatcher;
        private Action onSkillFiredAction;
        private Action onCooldownEndsAction;
        private Action onSkillEffectEndsAction;
        private VirtualKeyCode keyCode;
        private long castTimeTicks;
        private long cooldownTicks;
        private long skillLastsTicks;
        private TimeSpan cooldownInterval;
        private TimeSpan skillLastsInterval;

        public KeyCooldownTracker(PoetUI ui, IDispatcher dispatcher)
        {
            this.ui = ui;
            this.dispatcher = dispatcher;
        }

        public KeyCooldownTracker BindTo(VirtualKeyCode virtualKeyCode)
        {
            keyCode = virtualKeyCode;
            return this;
        }

        public KeyCooldownTracker CastTime(int castTimeMillisecond)
        {
            castTimeTicks = castTimeMillisecond * TimeSpan.TicksPerMillisecond;
            return this;
        }

        public KeyCooldownTracker Cooldown(int castTimeMillisecond)
        {
            cooldownTicks = castTimeMillisecond * TimeSpan.TicksPerMillisecond;
            return this;
        }

        public KeyCooldownTracker SkillLasts(int castTimeMillisecond)
        {
            skillLastsTicks = castTimeMillisecond * TimeSpan.TicksPerMillisecond;
            return this;
        }

        public KeyCooldownTracker WhenSkillFired(Action action)
        {
            onSkillFiredAction = action;
            return this;
        }

        public KeyCooldownTracker WhenSkillCooldownEnds(Action action)
        {
            onCooldownEndsAction = action;
            return this;
        }

        public KeyCooldownTracker WhenLinkedEffectEnds(Action action)
        {
            onSkillEffectEndsAction = action;
            return this;
        }

        public PoetUI Install()
        {
            cooldownInterval = new TimeSpan(castTimeTicks + cooldownTicks);
            skillLastsInterval = new TimeSpan(skillLastsTicks - castTimeTicks - cooldownTicks);

            // Make sure we are in the beginning state
            onSkillEffectEndsAction();

            ui.Bind(keyCode, Execute, KeyAction.Released);
            return ui;
        }

        private CancellationTokenSource tokenSource = new CancellationTokenSource();
        private CancellationToken token;
        private Task trackerTask;

        private void Execute(VirtualKeyCode pressedKey)
        {
            if (trackerTask?.Status == TaskStatus.WaitingForChildrenToComplete ||
                trackerTask?.Status == TaskStatus.Running)
            {
                tokenSource.Cancel();
                tokenSource = new CancellationTokenSource();
            }

            try
            {
                token = tokenSource.Token;
                trackerTask = Task.Factory.StartNew(TrackCooldown, token);
            }
            catch (TaskCanceledException)
            {
            }
        }

        private void TrackCooldown()
        {
            dispatcher.BeginInvoke(onSkillFiredAction);

            Thread.Sleep(cooldownInterval);
            dispatcher.BeginInvoke(onCooldownEndsAction);

            Thread.Sleep(skillLastsInterval);
            dispatcher.BeginInvoke(onSkillEffectEndsAction);
        }
    }
}