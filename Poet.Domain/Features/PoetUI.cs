﻿using System;
using System.Collections.Generic;
using System.Linq;
using WindowsInput.Native;
using LobitoApps.Poet.Domain.Keyboard;
using LobitoApps.Poet.Domain.RNG;

namespace LobitoApps.Poet.Domain.Features
{
    public class PoetUI: IEnableState, IRouteHotKeys, IDisposable
    {
        private readonly List<(VirtualKeyCode Key, KeyAction KeyEvent, Action<VirtualKeyCode> KeyAction)> bindings = 
            new List<(VirtualKeyCode Key, KeyAction KeyEvent, Action<VirtualKeyCode> KeyAction)>();

        private bool isEnabled = false;
        private readonly IDispatcher dispatcher;
        private readonly IKeyboardListener keyboardListener;

        private readonly IKeyboard keyboard;
        private readonly IRandomNumberGenerator rng;
        private CombatTracker combatTracker;

        public PoetUI(IDispatcher dispatcher, IKeyboardListener keyboardListener, IKeyboard keyboard, IRandomNumberGenerator rng)
        {
            this.dispatcher = dispatcher;
            this.keyboardListener = keyboardListener;
            this.keyboard = keyboard;
            this.rng = rng;
        }

        public bool IsEnabled() => isEnabled;
        public void Enable()
        {
            if (isEnabled) return;
            Subscribe();
            isEnabled = true;
        }

        public void Disable()
        {
            if (!isEnabled) return;
            Unsubscribe();
            isEnabled = false;
        }

        public void Toggle()
        {
            if (isEnabled)
                Disable();
            else
                Enable();
        }

        private void OnKeyPressed(object sender, KeyPressArgs e)
        {
            TryProcessKeyAction(e.KeyCode, KeyAction.Pressed);
        }

        private void OnKeyReleased(object sender, KeyPressArgs e)
        {
            TryProcessKeyAction(e.KeyCode, KeyAction.Released);
        }

        private void TryProcessKeyAction(VirtualKeyCode keyCode, KeyAction keyActionAction)
        {
            if (!IsEnabled()) return;

            var binding = bindings.SingleOrDefault(_ => _.Key == keyCode && _.KeyEvent == keyActionAction);
            if (binding.KeyAction == null) return;

            binding.KeyAction(keyCode);

            if (keyCode == VirtualKeyCode.VK_A || keyCode == VirtualKeyCode.VK_S)
            {
                combatTracker?.CombatActionDetected();
            }
        }

        public void Bind(VirtualKeyCode key, Action<VirtualKeyCode> action, KeyAction was)
        {
            bindings.Add((key, was, action));
        }

        public KeySpamMacro FeatureKeyMap()
        {
            return new KeySpamMacro(this, keyboard, rng);
        }

        public CommandMacro FeatureCommand()
        {
            return new CommandMacro(this, keyboard);
        }

        public KeyCooldownTracker FeatureCooldownTracker()
        {
            return new KeyCooldownTracker(this, dispatcher);
        }

        public CombatTracker FeatureCombatTracker()
        {
            combatTracker = new CombatTracker(this, dispatcher);
            return combatTracker;
        }

        private void Subscribe()
        {
            keyboardListener.OnKeyPressed += OnKeyPressed;
            keyboardListener.OnKeyReleased += OnKeyReleased;
        }

        private void Unsubscribe()
        {
            keyboardListener.OnKeyReleased -= OnKeyReleased;
            keyboardListener.OnKeyPressed -= OnKeyPressed;
        }

        public void Dispose()
        {
            Unsubscribe();
        }
    }
}