﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using LobitoApps.Poet.Domain.RNG;

namespace LobitoApps.Poet.Domain
{
    public static class LinqExtensionMethods
    {
        [DebuggerStepThrough]
        public static void Each<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (var element in source)
            {
                action(element);
            }
        }

        public static T Random<T>(this IEnumerable<T> enumerable)
        {
            var rng = RandomNumberGeneratorFactory.Create();
            var list = enumerable as IList<T> ?? enumerable.ToList();
            return !list.Any() ? default(T) : list.ElementAt(rng.Between(0, list.Count() - 1));
        }

        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> list)
        {
            var shuffledCopy = list.ToList();
            shuffledCopy.Shuffle();

            return shuffledCopy;
        }

        private static void Shuffle<T>(this IList<T> list)
        {
            var rng = RandomNumberGeneratorFactory.Create();
            var n = list.Count;

            while (n > 1)
            {
                n--;
                var k = rng.Next(n);
                var value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }
}
