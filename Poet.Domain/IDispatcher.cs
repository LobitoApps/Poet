﻿using System;

namespace LobitoApps.Poet.Domain
{
    public interface IDispatcher
    {
        void BeginInvoke(Action action);
    }
}