﻿using WindowsInput;
using WindowsInput.Native;

namespace LobitoApps.Poet.Domain.Keyboard
{
    public class KeyboardSimulator : IKeyboard
    {
        private readonly InputSimulator inputSimulator;

        public KeyboardSimulator()
        {
            inputSimulator = new InputSimulator();
        }

        public void Type(string text)
        {
            inputSimulator.Keyboard.TextEntry(text);
        }

        public void PressKey(VirtualKeyCode key, int delay)
        {
            inputSimulator
                .Keyboard.KeyDown(key)
                .Sleep(delay)
                .KeyUp(key);
        }
    }
}