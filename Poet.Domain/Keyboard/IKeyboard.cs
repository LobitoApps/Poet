﻿using WindowsInput.Native;

namespace LobitoApps.Poet.Domain.Keyboard
{
    public interface IKeyboard
    {
        void Type(string text);
        void PressKey(VirtualKeyCode key, int delay = 0);
    }
}