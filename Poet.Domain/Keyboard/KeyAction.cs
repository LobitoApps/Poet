﻿namespace LobitoApps.Poet.Domain.Keyboard
{
    public enum KeyAction
    {
        Pressed, Released
    }
}