using System;

namespace LobitoApps.Poet.Domain.Keyboard
{
    public interface IKeyboardListener
    {
        event EventHandler<KeyPressArgs> OnKeyPressed;
        event EventHandler<KeyPressArgs> OnKeyReleased;
    }
}