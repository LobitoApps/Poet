﻿using System.Security.Cryptography;
using System.Threading;

namespace LobitoApps.Poet.Domain.RNG
{
    public static class RandomNumberGeneratorFactory
    {
        private static readonly ThreadLocal<IRandomNumberGenerator> RandomWrapper =
            new ThreadLocal<IRandomNumberGenerator>(() => new RandomNumberGenerator(new RNGCryptoServiceProvider()));

        public static IRandomNumberGenerator Create()
        {
            return RandomWrapper.Value;
        }
    }
}