﻿namespace LobitoApps.Poet.Domain.RNG
{
    public interface IRandomNumberGenerator
    {
        int Next(int maximumValue);
        int Between(int minimumValue, int maximumValue);
        bool IsPercentageChance(int percent);
    }
}