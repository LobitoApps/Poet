﻿using System;
using System.Security.Cryptography;

namespace LobitoApps.Poet.Domain.RNG
{
    public class RandomNumberGenerator: IRandomNumberGenerator
    {
        private readonly RNGCryptoServiceProvider randomNumberGenerator;

        public RandomNumberGenerator(RNGCryptoServiceProvider randomNumberGenerator)
        {
            this.randomNumberGenerator = randomNumberGenerator;
        }

        public int Next(int maximumValue)
        {
            return Between(0, maximumValue);
        }

        public int Between(int minimumValue, int maximumValue)
        {
            // From https://scottlilly.com/create-better-random-numbers-in-c/
            byte[] randomNumber = new byte[1];
            randomNumberGenerator.GetBytes(randomNumber);

            double asciiValueOfRandomCharacter = Convert.ToDouble(randomNumber[0]);

            // We are using Math.Max, and substracting 0.00000000001, 
            // to ensure "multiplier" will always be between 0.0 and .99999999999
            // Otherwise, it's possible for it to be "1", which causes problems in our rounding.
            double multiplier = Math.Max(0, (asciiValueOfRandomCharacter / 255d) - 0.00000000001d);

            // We need to add one to the range, to allow for the rounding done with Math.Floor
            int range = maximumValue - minimumValue + 1;

            double randomValueInRange = Math.Floor(multiplier * range);

            return (int) (minimumValue + randomValueInRange);
        }

        public bool IsPercentageChance(int percent)
        {
            return Between(1, 100) <= percent;
        }
    }
}