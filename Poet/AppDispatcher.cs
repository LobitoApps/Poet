﻿using System;
using System.Windows.Threading;
using LobitoApps.Poet.Domain;

namespace LobitoApps.Poet
{
    public class AppDispatcher : IDispatcher
    {
        private readonly Dispatcher dispatcher;

        public AppDispatcher(Dispatcher dispatcher)
        {
            this.dispatcher = dispatcher;
        }

        public void BeginInvoke(Action action)
        {
            dispatcher.BeginInvoke(action);
        }
    }
}