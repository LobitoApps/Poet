﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Media;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Automation;
using WindowsInput.Native;
using LobitoApps.Poet.Domain.Features;
using LobitoApps.Poet.Domain.Keyboard;
using LobitoApps.Poet.Domain.RNG;
using Brush = System.Windows.Media.Brush;
using Brushes = System.Windows.Media.Brushes;
using Color = System.Windows.Media.Color;
using Point = System.Windows.Point;
using Size = System.Windows.Size;

namespace LobitoApps.Poet
{
    public partial class MainWindow
    {
        // Cooldown is within cast time + cooldown
        // Available is off cooldown but skill effect still active
        // Ready is off cooldown and should be recast for 100% uptime
        // So cycle goes Ready > Cooldown > Available > Ready
        private static Brush SkillReadyBrush => new SolidColorBrush(Color.FromRgb(254, 192, 118));
        private static Brush SkillReadyBackgroundBrush => new SolidColorBrush(Color.FromRgb(29, 12, 0));
        private static Brush SkillOnCooldownBrush => Brushes.Black;
        private static Brush SkillOnCooldownBackgroundBrush => Brushes.Transparent;
        private static Brush SkillAvailableBrush => Brushes.Black;
        private static Brush SkillAvailableBackgroundBrush => new SolidColorBrush(Color.FromRgb(29, 12, 0));

        static MainWindow()
        {
            SkillReadyBrush.Freeze();
            SkillReadyBackgroundBrush.Freeze();
            SkillOnCooldownBrush.Freeze();
            SkillOnCooldownBackgroundBrush.Freeze();
            SkillAvailableBrush.Freeze();
            SkillAvailableBackgroundBrush.Freeze();
        }

        private readonly PoetUI poet;
        private readonly Win32KeyboardListener keyboardListener;
        private readonly KeyboardSimulator keyboardSimulator;
        private readonly SoundPlayer flaskUpSoundPlayer = new SoundPlayer("./shortbeep.wav");

        private int flaskStackCounter;
        private int flaskCooldownMs = 6000;

        Timer autoKeyTimer;
        private const int gap = 5;
        private int gapper = gap;

        private void HealthTick(object state)
        {
            if (gapper == gap)
            {
                Application.Current.Dispatcher?.Invoke(() =>
                {
                    if (!poet.IsEnabled()) return;

                    using var bitmap = new Bitmap(1, 1);
                    using (var graphics = Graphics.FromImage(bitmap))
                    {
                        graphics.CopyFromScreen(new System.Drawing.Point(159, 1203), new System.Drawing.Point(0, 0), new System.Drawing.Size(1, 1));
                    }

                    var  p = bitmap.GetPixel(0, 0);

                    if (p.R != 162 || p.G != 27 || p.B != 37)
                    {
                        keyboardSimulator.PressKey(VirtualKeyCode.VK_Q, 17);
                    }
                });

                gapper--;
            }
            else
            {
                gapper--;
            }

            if (gapper == 0)
            {
                gapper = gap;
            }
        }

        public MainWindow()
        {
            InitializeComponent();

            keyboardListener = new Win32KeyboardListener();
            keyboardSimulator = new KeyboardSimulator();
            flaskUpSoundPlayer.Load();

            poet = new PoetUI(
                new AppDispatcher(Application.Current.Dispatcher),
                keyboardListener,
                keyboardSimulator,
                RandomNumberGeneratorFactory.Create());

            ShowReady(ButtonA);
            ShowReady(ButtonS);
            ShowReady(ButtonD);
            ShowReady(ButtonF);

            ConfigurePoetUI();

            AutomationFocusChangedEventHandler focusHandler = OnFocusChanged;
            Automation.AddAutomationFocusChangedEventHandler(focusHandler);

            autoKeyTimer = new Timer(HealthTick, null, 0, 250);
        }

        private void ConfigurePoetUI()
        {
            const int chanceMissFirst = 1;
            const int chanceMissLast = 1;
            const int chanceMissRandom = 1;

            const int chanceSwapFirst = 20;
            const int chanceSwapSecond = 10;
            const int chanceSwapLast = 20;

            poet
                .FeatureKeyMap()
                .BindTo(VirtualKeyCode.VK_W)
                .FiresWhen(KeyAction.Pressed)
                .RandomlyMissKeys(chanceMissFirst, chanceMissLast, chanceMissRandom)
                .RandomlySwapKeys(chanceSwapFirst, chanceSwapSecond, chanceSwapLast)
                .KeySequence(new[]
                {
                    //VirtualKeyCode.VK_1,
                    VirtualKeyCode.VK_2,
                    VirtualKeyCode.VK_3,
                    VirtualKeyCode.VK_4,
                    //VirtualKeyCode.VK_5,
                })
                .Install()

                // .FeatureKeyMap()
                // .BindTo(VirtualKeyCode.VK_Q)
                // .FiresWhen(KeyAction.Pressed)
                // .SingleKey(VirtualKeyCode.VK_1)
                // .Install()

                .FeatureKeyMap()
                .BindTo(new[] {VirtualKeyCode.VK_Q, VirtualKeyCode.VK_E})
                .FiresWhen(KeyAction.Pressed)
                .FlipFlop(VirtualKeyCode.VK_1, VirtualKeyCode.VK_5)
                .Install()

                .FeatureCommand()
                .BindTo(VirtualKeyCode.OEM_8) // backtick next to 1 on my keyboard
                .CommandText("/exit")
                .Install()

                .FeatureCommand()
                .BindTo(VirtualKeyCode.F3)
                .CommandText("/remaining")
                .Install()

                .FeatureCommand()
                .BindTo(VirtualKeyCode.F5)
                .CommandText("/hideout")
                .Install()

                .FeatureCommand()
                .BindTo(VirtualKeyCode.F6)
                .CommandText("/delve")
                .Install()

                .FeatureCombatTracker();
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            PositionWindow();
            keyboardListener.OnKeyPressed += OnKeyPressed;
            keyboardListener.HookKeyboard();
        }

        private void OnFocusChanged(object sender, AutomationFocusChangedEventArgs e)
        {
            var focusedElement = sender as AutomationElement;
            if (focusedElement == null) return;

            using var process = Process.GetProcessById(focusedElement.Current.ProcessId);

            if (process.ProcessName.Contains("Poet"))
            {
                return;
            }

            Application.Current.Dispatcher?.Invoke(new Action(() =>
            {
                if (process.ProcessName.Contains("PathOfExile"))
                {
                    poet.Enable();
                }
                else
                {
                    poet.Disable();
                }
            }));
        }

    private void OnClosing(object sender, EventArgs e)
        {
            keyboardListener.UnHookKeyboard();
            if (Visibility == Visibility.Visible)
            {
                keyboardListener.OnKeyPressed -= OnKeyPressed;
            }
        }

        private void PositionWindow()
        {
            Left = SystemParameters.PrimaryScreenWidth * 0.5 - Width * 0.5;
            Top = SystemParameters.FullPrimaryScreenHeight * 0.31 - Height * 0.5;
        }

        private void OnKeyPressed(object sender, KeyPressArgs e)
        {
            switch (e.KeyCode)
            {
                case VirtualKeyCode.VK_W:
                    if (poet.IsEnabled())
                    {
                        Dispatcher?.InvokeAsync(async () =>
                        {
                            Visibility = Visibility.Collapsed;
                            flaskStackCounter++;
                            await Task.Delay(flaskCooldownMs);
                            if (--flaskStackCounter <= 0)
                            {
                                Visibility = Visibility.Visible;
                                flaskUpSoundPlayer.Play();
                                flaskStackCounter = 0;
                            }
                        });
                    }
                    break;
                case VirtualKeyCode.SCROLL:
                    Application.Current.Shutdown();
                    break;
                // case VirtualKeyCode.PAUSE:
                //     poet.Toggle();
                //     Visibility = poet.IsEnabled() ? Visibility.Collapsed : Visibility.Visible;
                //     break;
            }
        }

        private void ShowOnCooldown(Button button)
        {
            button.Foreground = SkillOnCooldownBrush;
            button.Background = SkillOnCooldownBackgroundBrush;
        }

        private void ShowAvailable(Button button)
        {
            button.Foreground = SkillAvailableBrush;
            button.Background = SkillAvailableBackgroundBrush;
        }

        private void ShowReady(Button button)
        {
            button.Foreground = SkillReadyBrush;
            button.Background = SkillReadyBackgroundBrush;
        }

        private void OnClosed(object sender, EventArgs e)
        {
            poet.Dispose();
        }
    }
}
